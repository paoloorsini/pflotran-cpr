PFLOTRAN WITH CPR PRECONDITIONER
--------

This is a WIP project to add CPR-AMG functionallity to PFLOTRAN. This is a two stage preconditioner
which first applies algebraic multigrid to an extracted pressure system, then applies a simple
preconditioner to the whole system, which is corrected by the solution to the pressure system.


To run in CPR mode, the following options are available in the linear solver card,
e.g.,

    LINEAR_SOLVER FLOW
      # use cpr preconditioner:
      PC_TYPE CPR
      
      # Need to use quasi-Impes T1 extraction method
      # (as opposed to defualt full-Impes) if using
      # MPHASE process model, or anything with a thermal
      # governing equation
      # CPRQIMPES
       
      # fgmres is reccommended to use with cpr,
      # however we have seen good results without it:
      SOLVER_TYPE FGMRES
  
      # to adjust fill level of ILU solver used for stage 2
      # of the cpr preconditioner: (optional)
      # T2FILLIN 1
  
      # use this to change the linear solver of stage 1 of the
      # CPR from gmres to richardson. This may provide good speedup,
      # but has been known to break: (optional)
      # CPR1_TYPE RICHARDSON
      # Instead can try disabling the T1 linear solver (so just do AMG)
      # for a speedup: (optional)
      # CPRT1AMGONLY
  
      # this card should be completely analgous to HYPRE_OPTIONS
      # as standard in pflotran, but allows us to specify options 
      # to the AMG part of stage 1 of the cpr (which is part of
      # the hypre package): (optional)
      # CPR_HYPRE_OPTIONS
        #BOOMERAMG_STRONG_THRESHOLD 0.75
        #BOOMERAMG_COARSEN_TYPE PMIS
        #BOOMERAMG_INTERP_TYPE EXT+I
      #END
    END